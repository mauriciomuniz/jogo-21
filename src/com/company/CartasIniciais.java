package com.company;

public class CartasIniciais {

    public String carta1;
    public String carta2;

    public String getCarta1() {
        return carta1;
    }

    public void setCarta1(String carta1) {
        this.carta1 = carta1;
    }

    public String getCarta2() {
        return carta2;
    }

    public void setCarta2(String carta2) {
        this.carta2 = carta2;
    }
}
