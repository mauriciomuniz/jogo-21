package com.company;

import java.util.List;

public class IniciaJogo {

    public ListasCartas retornaDuasCartas() {

        Sortea sortea = new Sortea();
        BaralhoCompleto baralhoCompleto = new BaralhoCompleto();
        ListasCartas listasCartas = new ListasCartas();

        // TRAZ O BARALHO COMPLETO
        listasCartas.setBaralhoJogo(baralhoCompleto.baralho());

        listasCartas = sortea.primeirasCartas(listasCartas);

        return listasCartas;
    }



}
