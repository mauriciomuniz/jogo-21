package com.company;

import java.util.List;

public class ImprimeMensagem {

    public void inicio(ListasCartas listasCartas) {

        System.out.println("---------------------------------- ");
        System.out.println("--- Vamos começar a jogar 21!! --- ");
        System.out.println("---------------------------------- ");
        System.out.println("Essas são as suas cartas iniciais: " + listasCartas.getCartasIniciais() + " e a soma delas é: " + listasCartas.getSoma());

    }

    public void finaliza(ListasCartas listasCartas) {
        System.out.println("--- Sua pontuação final foi de: " + listasCartas.getSoma());
    }
}

