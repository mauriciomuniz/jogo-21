package com.company;

import java.util.List;

public class ListasCartas {

    public List<String> baralhoJogo;

    public List<String> cartasIniciais;

    public String cartasEscolhidas;

    public int soma;

    public List<String> getBaralhoJogo() {
        return baralhoJogo;
    }

    public void setBaralhoJogo(List<String> baralhoJogo) {
        this.baralhoJogo = baralhoJogo;
    }

    public List<String> getCartasIniciais() {
        return cartasIniciais;
    }

    public void setCartasIniciais(List<String> cartasIniciais) {
        this.cartasIniciais = cartasIniciais;
    }

    public String getCartasEscolhidas() {
        return cartasEscolhidas;
    }

    public void setCartasEscolhidas(String cartasEscolhidas) {
        this.cartasEscolhidas = cartasEscolhidas;
    }

    public int getSoma() {
        return soma;
    }

    public void setSoma(int soma) {
        this.soma += soma;
    }
}
