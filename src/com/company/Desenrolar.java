package com.company;

import java.util.List;

public class Desenrolar {

    public void doGame(ListasCartas listasCartas, boolean continuar) {
        Sortea sortea = new Sortea();
        IO io = new IO();

        while (continuar) {
            listasCartas = sortea.demaisCartas(listasCartas);
            System.out.println("A carta sorteado foi a: " + listasCartas.getCartasEscolhidas());

            if (listasCartas.getSoma() < 21) {
                System.out.println("--- Sua soma parcial é de: " + listasCartas.getSoma());
                continuar = io.verificaContinuidadeJogo();
            } else if (listasCartas.getSoma() == 21) {
                System.out.println("Você ganhou!!");
                continuar = false;
            } else if (listasCartas.getSoma() > 21) {
                System.out.println("Você perdeu!!");
                continuar = false;
            }
        }
    }
}
