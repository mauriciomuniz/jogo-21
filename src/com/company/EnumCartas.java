package com.company;

public enum EnumCartas {

    AS_DE_PAUS("Ás de paus", 1),
    DOIS_DE_PAUS("Dois de paus", 2),
    TRES_DE_PAUS("Tres de paus", 3),
    QUATRO_DE_PAUS("Quatro de paus", 4),
    CINCO_DE_PAUS("Cinco de paus", 5),
    SEIS_DE_PAUS("Seis de paus", 6),
    SETE_DE_PAUS("Sete de paus", 7),
    OITO_DE_PAUS("Oito de paus", 8),
    NOVE_DE_PAUS("Nove de paus", 9),
    DEZ_DE_PAUS("Dez de paus", 10),
    VALETE_DE_PAUS("Valete de paus", 10),
    DAMA_DE_PAUS("Dama de paus", 10),
    REIS_DE_PAUS("Reis de paus", 10),
    AS_DE_OUROS("Ás de ouros", 1),
    DOIS_DE_OUROS("Dois de ouros", 2),
    TRES_DE_OUROS("Tres de ouros", 3),
    QUATRO_DE_OUROS("Quatro de ouros", 4),
    CINCO_DE_OUROS("Cinco de ouros", 5),
    SEIS_DE_OUROS("Seis de ouros", 6),
    SETE_DE_OUROS("Sete de ouros", 7),
    OITO_DE_OUROS("Oito de ouros", 8),
    NOVE_DE_OUROS("Nove de ouros", 9),
    DEZ_DE_OUROS("Dez de ouros", 10),
    VALETE_DE_OUROS("Valete de ouros", 10),
    DAMA_DE_OUROS("Dama de ouros", 10),
    REIS_DE_OUROS("Reis de ouros", 10),
    AS_DE_COPAS("Ás de copas", 1),
    DOIS_DE_COPAS("Dois de copas", 2),
    TRES_DE_COPAS("Tres de copas", 3),
    QUATRO_DE_COPAS("Quatro de copas", 4),
    CINCO_DE_COPAS("Cinco de copas", 5),
    SEIS_DE_COPAS("Seis de copas", 6),
    SETE_DE_COPAS("Sete de copas", 7),
    OITO_DE_COPAS("Oito de copas", 8),
    NOVE_DE_COPAS("Nove de copas", 9),
    DEZ_DE_COPAS("Dez de copas", 10),
    VALETE_DE_COPAS("Valete de copas", 10),
    DAMA_DE_COPAS("Dama de copas", 10),
    REIS_DE_COPAS("Reis de copas", 10),
    AS_DE_ESPADAS("Ás de espadas", 1),
    DOIS_DE_ESPADAS("Dois de espadas", 2),
    TRES_DE_ESPADAS("Tres de espadas", 3),
    QUATRO_DE_ESPADAS("Quatro de espadas", 4),
    CINCO_DE_ESPADAS("Cinco de espadas", 5),
    SEIS_DE_ESPADAS("Seis de espadas", 6),
    SETE_DE_ESPADAS("Sete de espadas", 7),
    OITO_DE_ESPADAS("Oito de espadas", 8),
    NOVE_DE_ESPADAS("Nove de espadas", 9),
    DEZ_DE_ESPADAS("Dez de espadas", 10),
    VALETE_DE_ESPADAS("Valete de espadas", 10),
    DAMA_DE_ESPADAS("Dama de espadas", 10),
    REIS_DE_ESPADAS("Reis de espadas", 10);

    String carta;
    private int valor;

    EnumCartas(String carta, int valorCarta) {
        this.carta = carta;
        this.valor = valorCarta;
    }

    public static int getValor(String carta) {
        for (EnumCartas c : EnumCartas.values()) {
            if(c.getCarta().equals(carta)){
                return c.getValor();
            }
        }
        return 0;
    }

    public String getCarta() {
        return carta;
    }

    public int getValor() {
        return this.valor;
    }
}

