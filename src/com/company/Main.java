package com.company;

public class Main {

    public static void main(String[] args) {
        IniciaJogo iniciaJogo = new IniciaJogo();
        ImprimeMensagem imprimeMensagem = new ImprimeMensagem();
        IO io = new IO();
        Desenrolar desenrolar = new Desenrolar();
        boolean continuar;

        ListasCartas listasCartas = iniciaJogo.retornaDuasCartas();

        imprimeMensagem.inicio(listasCartas);

        continuar = io.verificaContinuidadeJogo();

        desenrolar.doGame(listasCartas, continuar);

        imprimeMensagem.finaliza(listasCartas);
    }
}
