package com.company;

import java.util.Scanner;

public class IO {
    boolean continuar = false;
    public boolean verificaContinuidadeJogo() {
        AcataOpcaoJogador acataOpcaoJogador = new AcataOpcaoJogador();

        System.out.println("");
        System.out.println("Quer mais uma carta?! [S para sim] ");

        Scanner entrada = new Scanner(System.in);
        String opcaoJogador = entrada.next();

        continuar = acataOpcaoJogador.acataOpcaoJogador(opcaoJogador);

        return  continuar;
    }
}