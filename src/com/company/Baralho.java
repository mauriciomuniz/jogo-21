package com.company;

public class Baralho {

    public String carta1;
    public String carta2;
    public String carta3;
    public String carta4;
    public String carta5;
    public String carta6;
    public String carta7;
    public String carta8;
    public String carta9;
    public String carta10;
    public String carta11;
    public String carta12;
    public String carta13;
    public String carta14;
    public String carta15;
    public String carta16;
    public String carta17;
    public String carta18;
    public String carta19;
    public String carta20;
    public String carta21;
    public String carta22;
    public String carta23;
    public String carta24;
    public String carta25;
    public String carta26;
    public String carta27;
    public String carta28;
    public String carta29;
    public String carta30;
    public String carta31;
    public String carta32;
    public String carta33;
    public String carta34;
    public String carta35;
    public String carta36;
    public String carta37;
    public String carta38;
    public String carta39;
    public String carta40;
    public String carta41;
    public String carta42;
    public String carta43;
    public String carta44;
    public String carta45;
    public String carta46;
    public String carta47;
    public String carta48;
    public String carta50;
    public String carta51;
    public String carta52;

    public String getCarta1() {
        return carta1;
    }

    public void setCarta1(String carta1) {
        this.carta1 = carta1;
    }

    public String getCarta2() {
        return carta2;
    }

    public void setCarta2(String carta2) {
        this.carta2 = carta2;
    }

    public String getCarta3() {
        return carta3;
    }

    public void setCarta3(String carta3) {
        this.carta3 = carta3;
    }

    public String getCarta4() {
        return carta4;
    }

    public void setCarta4(String carta4) {
        this.carta4 = carta4;
    }

    public String getCarta5() {
        return carta5;
    }

    public void setCarta5(String carta5) {
        this.carta5 = carta5;
    }

    public String getCarta6() {
        return carta6;
    }

    public void setCarta6(String carta6) {
        this.carta6 = carta6;
    }

    public String getCarta7() {
        return carta7;
    }

    public void setCarta7(String carta7) {
        this.carta7 = carta7;
    }

    public String getCarta8() {
        return carta8;
    }

    public void setCarta8(String carta8) {
        this.carta8 = carta8;
    }

    public String getCarta9() {
        return carta9;
    }

    public void setCarta9(String carta9) {
        this.carta9 = carta9;
    }

    public String getCarta10() {
        return carta10;
    }

    public void setCarta10(String carta10) {
        this.carta10 = carta10;
    }

    public String getCarta11() {
        return carta11;
    }

    public void setCarta11(String carta11) {
        this.carta11 = carta11;
    }

    public String getCarta12() {
        return carta12;
    }

    public void setCarta12(String carta12) {
        this.carta12 = carta12;
    }

    public String getCarta13() {
        return carta13;
    }

    public void setCarta13(String carta13) {
        this.carta13 = carta13;
    }

    public String getCarta14() {
        return carta14;
    }

    public void setCarta14(String carta14) {
        this.carta14 = carta14;
    }

    public String getCarta15() {
        return carta15;
    }

    public void setCarta15(String carta15) {
        this.carta15 = carta15;
    }

    public String getCarta16() {
        return carta16;
    }

    public void setCarta16(String carta16) {
        this.carta16 = carta16;
    }

    public String getCarta17() {
        return carta17;
    }

    public void setCarta17(String carta17) {
        this.carta17 = carta17;
    }

    public String getCarta18() {
        return carta18;
    }

    public void setCarta18(String carta18) {
        this.carta18 = carta18;
    }

    public String getCarta19() {
        return carta19;
    }

    public void setCarta19(String carta19) {
        this.carta19 = carta19;
    }

    public String getCarta20() {
        return carta20;
    }

    public void setCarta20(String carta20) {
        this.carta20 = carta20;
    }

    public String getCarta21() {
        return carta21;
    }

    public void setCarta21(String carta21) {
        this.carta21 = carta21;
    }

    public String getCarta22() {
        return carta22;
    }

    public void setCarta22(String carta22) {
        this.carta22 = carta22;
    }

    public String getCarta23() {
        return carta23;
    }

    public void setCarta23(String carta23) {
        this.carta23 = carta23;
    }

    public String getCarta24() {
        return carta24;
    }

    public void setCarta24(String carta24) {
        this.carta24 = carta24;
    }

    public String getCarta25() {
        return carta25;
    }

    public void setCarta25(String carta25) {
        this.carta25 = carta25;
    }

    public String getCarta26() {
        return carta26;
    }

    public void setCarta26(String carta26) {
        this.carta26 = carta26;
    }

    public String getCarta27() {
        return carta27;
    }

    public void setCarta27(String carta27) {
        this.carta27 = carta27;
    }

    public String getCarta28() {
        return carta28;
    }

    public void setCarta28(String carta28) {
        this.carta28 = carta28;
    }

    public String getCarta29() {
        return carta29;
    }

    public void setCarta29(String carta29) {
        this.carta29 = carta29;
    }

    public String getCarta30() {
        return carta30;
    }

    public void setCarta30(String carta30) {
        this.carta30 = carta30;
    }

    public String getCarta31() {
        return carta31;
    }

    public void setCarta31(String carta31) {
        this.carta31 = carta31;
    }

    public String getCarta32() {
        return carta32;
    }

    public void setCarta32(String carta32) {
        this.carta32 = carta32;
    }

    public String getCarta33() {
        return carta33;
    }

    public void setCarta33(String carta33) {
        this.carta33 = carta33;
    }

    public String getCarta34() {
        return carta34;
    }

    public void setCarta34(String carta34) {
        this.carta34 = carta34;
    }

    public String getCarta35() {
        return carta35;
    }

    public void setCarta35(String carta35) {
        this.carta35 = carta35;
    }

    public String getCarta36() {
        return carta36;
    }

    public void setCarta36(String carta36) {
        this.carta36 = carta36;
    }

    public String getCarta37() {
        return carta37;
    }

    public void setCarta37(String carta37) {
        this.carta37 = carta37;
    }

    public String getCarta38() {
        return carta38;
    }

    public void setCarta38(String carta38) {
        this.carta38 = carta38;
    }

    public String getCarta39() {
        return carta39;
    }

    public void setCarta39(String carta39) {
        this.carta39 = carta39;
    }

    public String getCarta40() {
        return carta40;
    }

    public void setCarta40(String carta40) {
        this.carta40 = carta40;
    }

    public String getCarta41() {
        return carta41;
    }

    public void setCarta41(String carta41) {
        this.carta41 = carta41;
    }

    public String getCarta42() {
        return carta42;
    }

    public void setCarta42(String carta42) {
        this.carta42 = carta42;
    }

    public String getCarta43() {
        return carta43;
    }

    public void setCarta43(String carta43) {
        this.carta43 = carta43;
    }

    public String getCarta44() {
        return carta44;
    }

    public void setCarta44(String carta44) {
        this.carta44 = carta44;
    }

    public String getCarta45() {
        return carta45;
    }

    public void setCarta45(String carta45) {
        this.carta45 = carta45;
    }

    public String getCarta46() {
        return carta46;
    }

    public void setCarta46(String carta46) {
        this.carta46 = carta46;
    }

    public String getCarta47() {
        return carta47;
    }

    public void setCarta47(String carta47) {
        this.carta47 = carta47;
    }

    public String getCarta48() {
        return carta48;
    }

    public void setCarta48(String carta48) {
        this.carta48 = carta48;
    }

    public String getCarta50() {
        return carta50;
    }

    public void setCarta50(String carta50) {
        this.carta50 = carta50;
    }

    public String getCarta51() {
        return carta51;
    }

    public void setCarta51(String carta51) {
        this.carta51 = carta51;
    }

    public String getCarta52() {
        return carta52;
    }

    public void setCarta52(String carta52) {
        this.carta52 = carta52;
    }
}
