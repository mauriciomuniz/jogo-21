package com.company;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class BaralhoCompleto {

    List<String> cartasBaralho = Arrays.asList("Ás", "Dois", "Tres", "Quatro", "Cinco", "Seis", "Sete", "Oito", "Nove", "Dez", "Valete", "Dama", "Reis");
    List<String> naipes = Arrays.asList(" de espadas", " de ouros", " de paus", " de copas");
    List<String> cartas = new ArrayList<String>();

    public List<String> baralho() {
        Baralho baralho = new Baralho();
        for (String carta : cartasBaralho) {
            for (String naipe : naipes) {
                cartas.add(carta + naipe);
            }
        }
        return cartas;
    }
}
