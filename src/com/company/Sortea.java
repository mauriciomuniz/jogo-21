package com.company;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Sortea {

    public ListasCartas primeirasCartas(ListasCartas listasCartas) {
        Random gerador = new Random();
        List<String> listaCartasIniciais = new ArrayList<>();

        for (int i = 0; i < 2; i++) {
            // SORTEA A CARTA
            String carta = listasCartas.getBaralhoJogo().get(gerador.nextInt(listasCartas.getBaralhoJogo().size()));

            // REMOVE A CARTA SORTEADA DO BARALHO
            listasCartas.getBaralhoJogo().remove(carta);

            // ADICIONA A LISTA DE CARTAS INICIAIS
            listaCartasIniciais.add(carta);

            // BUSCA O VALOR NUMERICO DA CARTA PARA REALIZAR A SOMA
            int j = EnumCartas.getValor(carta);
            listasCartas.setSoma(j);
        }
        listasCartas.setCartasIniciais(listaCartasIniciais);

        return listasCartas;
    }

    public ListasCartas demaisCartas(ListasCartas listasCartas) {

        Random gerador = new Random();

        // SORTEA CARTA
        String cartaEscolhida = listasCartas.getBaralhoJogo().get(gerador.nextInt(listasCartas.getBaralhoJogo().size()));

        // REMOVE CARTA SORTEADA DO BARALHO
        listasCartas.getBaralhoJogo().remove(cartaEscolhida);

        // BUSCA VALOR NUMERICO DA CARTA SORTEADA PARA REALIZAR A SOMA
        int i = EnumCartas.getValor(cartaEscolhida);
        listasCartas.setSoma(i);

        listasCartas.setCartasEscolhidas(cartaEscolhida);

        return listasCartas;
    }
}

